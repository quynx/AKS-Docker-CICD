FROM openjdk:8-jre-alpine
LABEL maintainer="abhishek.pradhan@citihub.com"
EXPOSE 8080/tcp
ADD build/libs/aks-accelerator.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]