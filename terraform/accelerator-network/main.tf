/*
Type: Resource Group
Purpose: To provide a container for the AKS network and its dependencies
*/
resource "azurerm_resource_group" "aks_vnet_rg" {
  location = "${var.location}"
  name     = "${format("%s-vnet-rg", var.project_name)}"
  count    = "${var.create_new_network ? 1 : 0}"
}

/*
Type: Virtual Network
Purpose: To provide a private network for AKS

Note:
The address space required for the cluster depends on the number of nodes in the cluster and the number of
pods per node. Also be aware that AKS runs its own pods by default (for operation, monitoring, logging).
A '/27' network is typically too small for Kubernetes, for example.
*/
resource "azurerm_virtual_network" "aks_vnet" {
  location            = "${var.location}"
  name                = "${format("%s-vnet", var.project_name)}"
  resource_group_name = "${azurerm_resource_group.aks_vnet_rg[0].name}"
  address_space       = ["10.10.10.0/24"]
  count               = "${var.create_new_network ? 1 : 0}"
}


/*
Type: Subnet
Purpose: To provide a private network for AKS
*/
resource "azurerm_subnet" "aks_subnet" {
  address_prefixes       = ["10.10.10.0/24"]
  name                 = "${format("%s-vnet-sn1", var.project_name)}"
  resource_group_name  = "${azurerm_resource_group.aks_vnet_rg[0].name}"
  virtual_network_name = "${azurerm_virtual_network.aks_vnet[0].name}"
  count                = "${var.create_new_network ? 1 : 0}"
}
