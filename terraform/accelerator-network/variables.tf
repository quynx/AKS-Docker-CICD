variable "location" {
    default = "eastasia"
}
variable "project_name" {
    default = "aks-accelerator"
}
variable "create_new_network" {
    default = true
}
