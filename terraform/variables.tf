variable "location" {
  default = "eastasia"
}

variable "project_name" {
  default = "aks-accelerator"
}

variable "existing_subnet_id" {
  default = ""
}

variable "tags" {
  type = map(string)

  default = {
    "terraform" = true
  }
}

variable "service_principal" {
  default = "7e82949a-0d70-4804-8733-9d65452b4193"
}
variable "service_principal_password" {
  default = "LAo8Q~x9MBa2YrH0iJYJMtTpJ0Zrb7xY1_NYVboR"
}
variable "create_new_network" {
  default = true
}
