# 06 - Connecting Gitlab to AKS
_Aim: To publish a “Hello World” application from Gitlab to AKS._

# 6.0 Introduction
In this section we will configure our AKS cluster in Gitlab, then deploy Gitlab Runner so that we can use our cluster both for CI and and as a deployment target (CD).

## 6.1 Adding an Existing Kubernetes Cluster to Gitlab

In your Gitlab project, navigate to _Operations > Kubernetes > Add Existing Cluster_:

![gitlab kubernetes integration](./img/05_gitlab_kubernetes.png "Adding a Kubernetes Cluster to Gitlab")

Using this form, we will supply the credentials we need to reach our Kubernetes cluster (We will demonstrate how to obtain this information in the sub-sections below). [Gitlab's documentation](https://gitlab.com/help/user/project/clusters/index#adding-an-existing-kubernetes-cluster) provides additional information on the values for these fields.

## 6.2 Required Configuration Parameters

| Required Field | Example |
| --- | --- |
| **Kubernetes cluster name** | `aks-accelerator-aks` |  
| **Environment scope** | `staging` |
| **API URL** | https://aks-accelerator-xxxxxxxx.hcp.$REGION.azmk8s.io:443 |
| **CA Certificate** | A PEM (plaintext) x509 certificate | 
| **Token** | The token for a Kubernetes `ServiceAccount` called `gitlab-admin` that has been granted the `cluster-admin` role | 
| **Project namespace (optional, unique)** | Can be left blank to ask Gitlab to create a Kubernetes namespace for the project | 
| **RBAC-enabled cluster** | Specified when creating the cluster | Yes |


### 6.2.1 Kubernetes Cluster Name
Specified when creating the cluster.

Can be retrieved with the Azure CLI (see `Name`):

```
➜  az aks list --o=table

Name                 Location    ResourceGroup       KubernetesVersion    ProvisioningState    Fqdn
-------------------  ----------  ------------------  -------------------  -------------------  -------------------------------------------------
aks-accelerator-aks  westeurope  aks-accelerator-rg  1.12.6               Succeeded            aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io
```

Or with `kubectl`:

```
➜  kubectl config current-context

aks-accelerator-aks
```

### 6.2.2 Environment Scope
_Environment Scope_ is used to tell Gitlab how to use the Cluster (e.g. for Dev, Staging, Prod or for all workloads).

Although Gitlab documentation suggests that this as a Premium feature (hinting that, in the Community Edition, we should default to `*`), it actually appears to be mandatory. If we set `*`, Gitlab appears **not** to expose its environment variables (`$KUBE_CONFIG`, etc.) to our CI/CD pipeline.

For the purposes of this repo, we have declared `staging`.

Later (see [08 Deploying a Container Image with Gitlab CI/CD](08_gitlab_ci_deploy_image.md)), we will ensure that `.gitlab-ci.yml` declares the environment specified for the cluster, e.g.

```
  environment:
    name: staging
```

> Note: See [Troubleshooting Missing KUBECONFIG or KUBE_TOKEN](https://gitlab.com/help/user/project/clusters/index#troubleshooting-missing-kubeconfig-or-kube_token) (Gitlab Docs)


### 6.2.3 API URL
The Kubernetes API server, visible as the `fqdn` in `az aks list --o=table` (above). Remember to prefix with `https:`.

> Note that AKS API Server endpoints are public, regardless of the use of 'Advanced Networking'.


### 6.2.4 CA Certificate
The AKS API Server endpoint is secured with a self-signed TLS certificate.
Gitlab must be provided with the CA cert so that it can trust it.

Following Gitlab's instructions:

- List the available secrets with `kubectl get secrets`; one should have name matching `default-token-xxxxx`

```
➜  kubectl get secrets

NAME                  TYPE                                  DATA   AGE
default-token-abcde   kubernetes.io/service-account-token   3      24h
```

- Using `kubectl get secret` and the name of this token, lookup the certificate in the config map, and decode:

```
kubectl get secret default-token-abcde -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

-----BEGIN CERTIFICATE-----
...
-----END CERTIFICATE-----

```

### 6.2.5 Token
GitLab authenticates against Kubernetes using service tokens, which are scoped to a particular namespace.
The token used should belong to a service account with cluster-admin privileges.
The ".yaml" files referenced below can be found in the `/kubernetes` folder of this repository.

- Create a `ServiceAccount` called `gitlab-admin`:

`kubectl apply -f gitlab-admin-service-account.yaml`

- Grant `ClusterRole` `cluster-admin`:

`kubectl apply -f gitlab-admin-cluster-role-binding.yaml`

- Acquire the token for the `gitlab-admin` `ServiceAccount`:

`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')`

```
Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1720 bytes
namespace:  11 bytes
token:      <token string>
```

> Note: Please be ensure that you are aware of the security implications of doing this - Gitlab is being granted administrative access to your cluster. See [Gitlab's documentation](https://gitlab.com/help/user/project/clusters/index#security-implications) for a similar warning.

Copy the "token string", making sure not to copy any extraneous spaces.

#### A note on Authentication

At the time of writing, although not mentioned in the Gitlab documentation, it was also necessary to grant the `cluster-admin` `ClusterRole` to the `default` `ServiceAccount` in the `gitlab-managed-apps` namespace.

```
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-managed-apps-admin
subjects:
- kind: ServiceAccount
  name: default
  namespace: gitlab-managed-apps
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
```

(This is already included in `/kubernetes/gitlab-admin-cluster-role-binding.yaml` in this repository.)

> Note: Follow [this Gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/55362) for further developments relating to cluster authentication.

### 6.2.6. Project namespace

Kubernetes namespaces are a way to segregate projects within a single Kubernetes cluster. For this example we'll have Gitlab create one for us, but if you have set up a specific namespace in Kubernetes you can enter it here:

> Gitlab documentation notes that you don't have to fill it in; by leaving it blank, GitLab will create one for you

Later, we will also need to grant Gitlab access to the particular namespace it creates for us in the same fashion. A template for this also exists in the repository. See `template-aks-accelerator-cluster-role-binding.yaml`.


### 6.2.7 RBAC-enabled cluster
In this lab, we've created an RBAC enabled cluster, so we should mark this as RBAC enabled.


## 6.3 Installing Tiller & Gitlab Runner

After we have configured our credentials, we must prepare the cluster for use, installing:

- [Helm Tiller](https://helm.sh)
- Gitlab Runner

### 6.3.1 Tiller
Helm is _the package manager for Kubernetes_.

It allows for the creation of complex sets of Kubernetes manifests that are packaged up as a "Chart" and installed with `helm install`. `Tiller` is the cluster-side component of Helm v2 (it will be removed in Helm v3).

To install Tiller, click the Install button:

![Installing Tiller](./img/05_Installing_Tiller.png "Installing Tiller")


### 6.3.2 Gitlab Runner

Once Tiller is installed, we can install Gitlab Runner on our cluster, which will then let us deploy to applications to our cluster from Gitlab CICD.

![Installing Gitlab Runner](./img/05_Installing_Gitlab_Runner.png "Installing Gitlab Runner")

### 6.3.3 Troubleshooting

Sometimes, Gitlab can fail to install Tiller, which is a blocker. See [10 Troubleshooting](10_troubleshooting_deployments.md) for more information.

If a new cluster has been created against an existing project, be aware that old Gitlab Runners are not deleted, but will be left orphaned. This may affect your builds. If it happens, navigate to _Settings > CI / CD > Runners_ to delete them.

#### tl;dr
If you cannot use Helm, try the following:

```
kubectl -n kube-system patch deployment tiller-deploy -p '{"spec": {"template": {"spec": {"automountServiceAccountToken": true}}}}'
```

## Next
In the next section, we will manually deploy a "Hello World" Application to AKS.

< [04 Creating an AKS Cluster](04_I_creating_aks_via_terraform.md) | 05 Connecting Gitlab to AKS | [06 Manually Deploying a "Hello World" Application to AKS](06_deploying_to_aks.md) >