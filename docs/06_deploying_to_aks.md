# 06 - Manually Deploying a "Hello World" Application to AKS
_Aim: To deploy a “Hello World” application from to AKS._

## 6.0 Introduction
We can either deploy to Kubernetes _imperatively_ (by passing commands to `kubectl`) or _declaratively_ (by declaring the state that we would like Kubernetes to achieve and maintain).

In most cases, we would prefer to have a declarative configuration. This is where Kubernetes manifests come in.

## 6.1 Defining our Deployment

A _Deployment_ provides declarative state for Pods and ReplicaSets, see the [Kubernetes documentation](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

A manifest like this:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: aks-accelerator-deployment
  labels:
    app: accelerator
spec:
  replicas: 2
  selector:
    matchLabels:
      app: accelerator
  template:
    metadata:
      labels:
        app: accelerator
    spec:
      containers:
      - name: accelerator
        image: registry.gitlab.com/citihub/aks-accelerator:latest
        ports:
        - containerPort: 80
```

Will do the following:

- Create a Kubernetes `Deployment` named `aks-accelerator-deployment`, indicated by the `.metadata.name` field
- The Deployment will create two replicated `Pods` (indicated by the `replicas` field)
- The `selector` field defines which `Pods` will be managed by the `Deployment`:

```
  selector:
    matchLabels:
      app: accelerator
```

- We ask the `Deployment` to select `Pods` that have the `app: accelerator` label
- We label our `Pods` with the following `template`:

```
  template:
    metadata:
      labels:
        app: accelerator
```

- Each of our Pods contains a single container (recall that a Kubernetes `Pod` is an abstraction that wraps one or more containers)
- We call this container `accelerator`
```
  template:
    ...
    spec:
      containers:
      - name: accelerator
        image: registry.gitlab.com/citihub/aks-accelerator:latest
        ports:
        - containerPort: 8080
```


## 6.2 Creating our Deployment

We apply our desired state configuration:

```
➜  kubectl apply -f deployment.yaml

deployment.apps/aks-accelerator-deployment created
```

And then `watch` as our two Pods are created (initially in `ContainerCreating` state, in `Running` state a few moments later).

```
➜  kubectl get pods --watch

NAME                                          READY   STATUS              RESTARTS   AGE
aks-accelerator-deployment-65cf4464dd-fs6bx   0/1     ContainerCreating   0          13s
aks-accelerator-deployment-65cf4464dd-j5dhh   0/1     ContainerCreating   0          13s
aks-accelerator-deployment-65cf4464dd-fs6bx   1/1   Running   0     16s
aks-accelerator-deployment-65cf4464dd-j5dhh   1/1   Running   0     17s
```


## 6.3 Creating a Service to Expose our Application

We will not be able to reach our application yet. Whilst our Pods are running, they are only accessible from other Pods within the cluster.

Kubernetes permits sophisticated control of access to the applications it is running via an `Ingress`.

To make an initial test of our application, we can expose our application via a simple `Service` of `type=LoadBalancer`. This does not permit sophisticated control of which traffic reaches which of our pods, but it is a sensible initial test.

```
apiVersion: v1
kind: Service
metadata:
  name: aks-accelerator-service
spec:
  selector:
    app: accelerator
  type: LoadBalancer
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```


This specification will:

- Create a new `Service` object named `aks-accelerator-service`
- Which targets TCP port 8080
- On any Pod with the "app=accelerator" label

To apply it:

```
➜  kubectl apply -f service-public.yaml

service/aks-accelerator-service created
```

## 6.4 Connecting to Our Application
We ask Kubernetes where our application lives:

```
➜  kubectl get services

NAME                      TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
aks-accelerator-service   LoadBalancer   10.0.10.62    50.123.45.67   80:31037/TCP   23h
```

```
➜  ~ curl -X GET http://50.123.45.67:32769
Just hello world. Application demonstrates Gitlab, K8s integration.
```


## 6.5 Exposing an Internal Load Balancer

To make our Load Balancer private - i.e. with an IP assigned from our Virtual Network (as opposed to a public IP) - we add the following annotation:

```
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
``` 

> Note: The Azure documentation for this is [here](https://docs.microsoft.com/en-us/azure/aks/internal-lb).

- If no IP is specified, one will be allocated by AKS
- To specify an IP - perhaps for DNS association - add the following to the Service spec:

```
spec:
  type: LoadBalancer
  loadBalancerIP: 10.240.0.25
```

To apply it:

```
➜  kubectl apply -f service-private.yaml
```

To quote Microsoft's documentation:

> Note: The specified IP address must reside in the same subnet as the AKS cluster and must not already be assigned to a resource.

### 6.5.1 Load Balancer Reachability

Load Balancers created by AKS are the _Basic_ SKU - this is not something that can currently be controlled by Kubernetes.

Ordinarily, this is fine, but in some network topologies it can cause a problem.

Microsoft typically recommend a Hub - Spoke model for enterprise Azure networks, where Express Route terminates in the Hub, and the Hub is in turn peered to a separate Spoke Virtual Network.

Specifically, a _Basic_ Load Balancer does not support a network topology in which the Hub Virtual Network and Spoke Virtual Network are in different regions. If you have trouble reaching an application behind an Azure Load Balancer, and you have a topology like this, keep it in mind.


## Next
In the next section we will build our container image using Gitlab CI/CD.


< [05 Connecting Gitlab to AKS](06_connecting_gitlab_to_aks.md) | 06 Manually Deploying a "Hello World" Application to AKS| [07 Building a Container Image with Gitlab CI/CD](07_gitlab_ci_build_image.md) > 
